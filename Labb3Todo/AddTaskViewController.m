//
//  AddTaskViewController.m
//  Labb3Todo
//
//  Created by IT-Högskolan on 2015-02-05.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "AddTaskViewController.h"
#import "TaskClass.h"

@interface AddTaskViewController ()
@property (weak, nonatomic) IBOutlet UITextField *addThingTodo;

@end

@implementation AddTaskViewController
- (IBAction)addButtonPressed:(id)sender {
    [self.todo2 addObject:self.addThingTodo.text];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
