//
//  TaskClass.h
//  Labb3Todo
//
//  Created by IT-Högskolan on 2015-02-05.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TaskClass : NSObject

@property (nonatomic)NSMutableArray *Todo;
- (void)removeTodoObject:(NSString *)object;
- (void)addTodoObject:(NSString *)object;
@end
