//
//  TaskClass.m
//  Labb3Todo
//
//  Created by IT-Högskolan on 2015-02-05.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "TaskClass.h"

@implementation TaskClass

-(NSArray*) Todo {
    if (!_Todo) {
        _Todo = [@[@"Städa", @"Laga Mat", @"Dricka Öl", @"Diska", @"Tvätta"]mutableCopy];
    }
    return _Todo;
}

-(void) addTodoObject:(NSString *)object {
    [_Todo addObject:object];
}
-(void)removeTodoObject:(NSString *)object {
    [_Todo removeObject:object];
}

- (void)setCollection:(NSMutableArray *)array
{
    
}

- (void)addToCollection:(NSMutableArray *)array
{
    // your code here
}

@end
